import React from 'react';
import Web3 from 'web3'
import logo from './logo.svg';
import './App.css';
import Asset from './Asset.json'


class App extends React.Component {

  async componentWillMount(){
    await this.loadWeb3()
    await this.loadBlockchainData()
  }

  async loadWeb3(){
    if(window.ethereum){
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable()
    }
    else if(window.web3){
      window.web3 = new Web3(window.web3.currentProvider)
    }
    else{
      window.alert('Non-Ethereum browser detected. You should consider using Metamask')
    }
  }

  async loadBlockchainData(){
    const web3 = window.web3
    //Load accounts
    const accounts = await web3.eth.getAccounts()
    this.setState({account: accounts[0]})

    const networkId = await web3.eth.net.getId()
    const networkData = Asset.networks[networkId]
    if(networkData){
      // Load Contract
      const abi = Asset.abi
      const contractAddress = networkData.address
      const assetContract = new web3.eth.Contract(abi,contractAddress )
      console.log(assetContract)
      this.setState({contract: assetContract})

      // Load totalSupply
      const totalSupply = await assetContract.methods.totalSupply().call()
      this.setState({totalSupply})

      // Load Assets
      for (var i=1 ; i<= totalSupply ; i++){
        const asset = await assetContract.methods.assets(i - 1).call()
        this.setState({
          assets: [...this.state.assets, asset]
        })
      }
      console.log(this.state.assets)

    }
    else{
      window.alert('Smart contract not deployed on the network')
    }
  }

  mint = (asset) => {
    console.log(asset)
    this.state.contract.methods.mint(asset).send({from: this.state.account}).once('receipt',(receipt) =>{
      this.setState({
        assets: [...this.state.assets, asset]
      })
    })
  }

  constructor(props){
    super(props);
    this.state = {
      account: '',
      contract: null,
      totalSupply :0,
      assets: []
    }
  }


  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>React Blockchain client</h1>
        </header>
        <div className="App-content">
          <p>This is a test application to connect to GANACHE.</p>
          <p>Account:{this.state.account}</p>
          <p>Form Mint new token.</p>
          <div>
          <form onSubmit={(event) =>{
            event.preventDefault()
            const asset = this.asset.value
            this.mint(asset)
          }}>
          <input type='text' ref={(input) => {this.asset = input}}/>
          <input type='submit' value ='MINT' />
          </form>
          </div>
          <p>List all Assets.</p>
          <div>
            { this.state.assets.map((asset, key) => {
              return(
                <div key={key}>
                <div></div>
                <div>{asset}</div>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
