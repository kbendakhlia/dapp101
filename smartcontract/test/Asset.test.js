const Asset = artifacts.require('./Asset.sol')

require('chai')
  .use(require('chai-as-promised'))
  .should()

contract('Asset', (accounts) => {
  let contract

  before(async () =>{
    contract = await Asset.deployed()
  })

  describe('deployment', async() => {

    it('deploys successfully', async () => {
      const address = contract.address
      //console.log('Contract address: '+address)
      assert.notEqual(address,0x0)
      assert.notEqual(address,'')
      assert.notEqual(address,null)
      assert.notEqual(address,undefined)
    })

    it('has a name', async () => {
      const name = await contract.name()
      //console.log('Contract name: '+name)
      assert.equal(name,'Asset')
    })

    it('has a symbol', async () => {
      const symbol = await contract.symbol()
      //console.log('Contract symbol: '+symbol)
      assert.equal(symbol,'AST')
    })

  })

  describe('minting', async() => {

    it('creates a new token', async () => {
      const result = await contract.mint('#EC058E')
      const totalSupply = await contract.totalSupply()
      // SUCCESS
      assert.equal(totalSupply,1)
      //console.log(result)

      const event = result.logs[0].args
      assert.equal(event.tokenId.toNumber(),1,'id is correct')
      assert.equal(event.from,'0x0000000000000000000000000000000000000000','from is correct')
      assert.equal(event.to, accounts[0], 'to is correct')
    })
  })

  describe('indexing', async() => {

    it('lists assets', async () => {
      await contract.mint('#000000')
      await contract.mint('#FFFFFF')
      await contract.mint('#AAAAAA')
      const totalSupply = await contract.totalSupply()

      let asset
      let result = []

      for (var i=1 ; i<= totalSupply ; i++){
        asset = await contract.assets(i - 1)
        result.push(asset)
      }

      let expected = ['#EC058E','#000000','#FFFFFF','#AAAAAA']
      assert.equal(result.join(','),expected.join(','))
    })
  })




})
