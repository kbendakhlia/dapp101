pragma solidity ^0.5.0;

import "@openzeppelin/contracts/token/ERC721/ERC721Full.sol";

contract Asset is ERC721Full {

    string[] public assets;

    mapping(string => bool) _assetExists;

    constructor() ERC721Full("Asset", "AST") public {
    }

    function mint(string memory _assetid) public  {

      require(!_assetExists[_assetid]);
      uint _id = assets.push(_assetid);
      _mint(msg.sender, _id);
      _assetExists[_assetid] = true;

    }

}
